module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      fontFamily: {
        display: ["Muli", "sans-serif"],
        body: ["Muli", "sans-serif"],
        sans: ["Muli", "sans-serif"],
      },
      colors: {
        brand: {
          blue: "#02b4e0",
          "blue-alt": "#00cefc",
          green: "#2fc247",
          "green-alt": "#72ed84",
          gray: "#3c4b55",
          "gray-alt": "#8195a3",
          gray2: "#596771",
        },
      }
    },
  },
  plugins: [],
}
