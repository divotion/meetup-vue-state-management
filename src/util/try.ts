import { getCurrentScope, onScopeDispose } from 'vue';

export function tryOnScopeDispose(fn: () => void): boolean {
    if (getCurrentScope()) {
        onScopeDispose(fn);
        return true;
    }

    return false;
}