export function format(n: number, decimals = 2): number {
    // return n;
    return +n.toFixed(decimals);
}