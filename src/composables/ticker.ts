import { Pausable, useRaf } from './requestAnimationFrame';

export function useTicker(tick: (delta: number) => unknown): Pausable {
    let previousTick = Date.now();

    return useRaf(() => {
        const now = Date.now();
        const delta = now - previousTick;

        previousTick = now;

        tick(delta);
    });
}
