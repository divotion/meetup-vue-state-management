import {ref, Ref, UnwrapRef, watch} from 'vue';

export function useLocalStorage<T extends object>(
    key: string,
    initialValue: T,
    {
        read = (data: any) => data,
        write = (data: any) => data
    } = {}
): [T] extends [Ref] ? T : Ref<UnwrapRef<T>> {
    const data = ref(initialValue);
    const storage = window?.localStorage;

    if (storage) {
        const rawValue = storage[key];

        if (rawValue) {
            read(JSON.parse(rawValue));
        }

        watch(data, () => {
            storage[key] = JSON.stringify(write(data.value));
        }, {
            immediate: !rawValue,
            deep: true,
        });
    }

    return data;
}
