import {ref, Ref} from 'vue';
import {isClient} from "../util/is";
import {tryOnScopeDispose} from "../util/try";

export interface Pausable {
    isActive: Ref<boolean>;
    pause: () => void;
    resume: () => void;
}

export function useRaf(fn: () => void): Pausable {
    const isActive = ref(false);

    function loop() {
        if (!isActive.value || !isClient) {
            return;
        }

        fn();
        requestAnimationFrame(loop);
    }

    function pause() {
        isActive.value = false;
    }

    function resume() {
        if (isActive.value || !isClient) {
            return;
        }

        isActive.value = true;
        loop();
    }

    resume();
    tryOnScopeDispose(pause);

    return {
        isActive,
        pause,
        resume,
    };
}