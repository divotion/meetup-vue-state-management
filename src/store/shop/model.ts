import { ComputedRef, Ref, UnwrapRef } from 'vue';

export interface Worker {
    name: string;
    divotionPerClick: number;
    divotionPerSecond: number;
}

export interface ShopItem {
    price: ComputedRef<number>;
    owned: Ref<number>;
    worker: Worker;
}

export interface ShopStore {
    inventory: Ref<UnwrapRef<ShopItem>[]>;
    buy(item: UnwrapRef<ShopItem>): void;
    sell(item: UnwrapRef<ShopItem>): void;
}
