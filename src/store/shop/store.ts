import { computed, ref, reactive, UnwrapRef, UnwrapNestedRefs } from 'vue';
import {useDivotion} from "../divotion";
import { ShopStore, ShopItem } from './model';
import {useLocalStorage} from "../../composables/localStorage";

const { divotion, divotionPerClick, divotionPerSecond } = useDivotion();

const inventory = ref([
    createShopItem(1, {
        name: 'Workaholic',
        divotionPerClick: 0,
        divotionPerSecond: .1,
    }),
    createShopItem(1, {
        name: 'Outsourcing',
        divotionPerClick: 0,
        divotionPerSecond: 1,
    }),
    createShopItem(1, {
        name: 'Manager',
        divotionPerClick: 0,
        divotionPerSecond: 7,
    }),
]);

function createShopItem(basePrice: number, worker: Worker): UnwrapNestedRefs<ShopItem> {
    const owned = ref(0);
    const price = computed(() => (1 + owned.value) * basePrice);
    return reactive({ price, owned, worker: reactive(worker) });
}

useLocalStorage('divotion.game.shop', inventory, {
    read: (data: Record<string, number>) => {
        for (const name of Object.keys(data)) {
            const item = inventory.value.find((i) => i.worker.name === name);

            if (item) {
                item.owned = data[name];
            }
        }

        return inventory;
    },
    write: (data: UnwrapNestedRefs<ShopItem>[]) => data.reduce((data: Record<string, number>, item) => {
        data[item.worker.name] = item.owned;
        return data;
    }, {}),
});

function buy(item: UnwrapRef<ShopItem>): void {
    const buyPrice = item.price;

    if (divotion.value >= buyPrice) {
        divotion.value -= buyPrice;
        item.owned++;

        divotionPerClick.value += item.worker.divotionPerClick;
        divotionPerSecond.value += item.worker.divotionPerSecond;
    }
}

function sell(item: UnwrapRef<ShopItem>): void {
    const sellPrice = (item.price / item.owned) - 1;

    if (item.owned > 0) {
        divotion.value += sellPrice;
        item.owned--;

        divotionPerClick.value -= item.worker.divotionPerClick;
        divotionPerSecond.value -= item.worker.divotionPerSecond;
    }
}

export function useShop(): ShopStore {
    return {
        inventory,
        sell,
        buy,
    };
}