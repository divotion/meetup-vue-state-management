export interface Notification {
    title: string;
    content: string;
    timeout?: number;
}
