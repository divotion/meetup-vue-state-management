import { ref } from 'vue';
import { Notification } from './model';

const notifications = ref<Notification[]>([]);

function show(notification: Notification) {
    notifications.value.push(notification);

    if (notification.timeout != null) {
        setTimeout(() => close(notification), notification.timeout);
    }
}

function close(notification: Notification) {
    const index = notifications.value.indexOf(notification);

    if (index >= 0) {
        notifications.value.splice(index, 1);
    }
}

export function useNotifications() {
    return {
        notifications,
        show,
        close,
    };
}