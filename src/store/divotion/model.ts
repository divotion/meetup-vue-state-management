import { Ref } from 'vue';

export interface DivotionStorage {
    divotion: Ref<number>;
    divotionAllTime: Ref<number>;
    divotionPerClick: Ref<number>;
    divotionPerSecond: Ref<number>;
}

export interface DivotionStore extends DivotionStorage {
    click(): void;
    tick(delta: number): void;
}
