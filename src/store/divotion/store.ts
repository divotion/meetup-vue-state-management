import { DivotionStore } from './model';
import {ref} from "vue";
import {useLocalStorage} from "../../composables/localStorage";

const divotion = ref(0);
const divotionAllTime = ref(0);
const divotionPerSecond = ref(0);
const divotionPerClick = ref(1);

function click() {
    const amount = divotionPerClick.value;
    divotion.value += amount;
    divotionAllTime.value += amount;
}

function tick(delta: number) {
    const amount = divotionPerSecond.value * (delta / 1000);
    divotion.value += amount;
    divotionAllTime.value += amount;
}

useLocalStorage('divotion.game.divotion', {
    divotion,
    divotionPerClick,
    divotionPerSecond,
    divotionAllTime,
}, {
    read: (data) => {
        divotion.value = data.divotion;
        divotionPerClick.value = data.divotionPerClick;
        divotionPerSecond.value = data.divotionPerSecond;
        divotionAllTime.value = data.divotionAllTime;
    },
});

export function useDivotion(): DivotionStore {
    return {
        divotion,
        divotionPerClick,
        divotionPerSecond,
        divotionAllTime,
        click,
        tick,
    };
}