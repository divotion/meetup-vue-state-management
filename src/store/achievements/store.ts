import { computed, ref, watch } from 'vue';
import { useDivotion } from "../divotion";
import { useShop } from "../shop";
import {Achievement} from "./model";
import {useNotifications} from "../notifications";

const { inventory } = useShop();
const { divotionAllTime } = useDivotion();
const { show } = useNotifications();

const achievements = ref([
    createAchievement(
        'Divotion',
        'Earn your first divotion',
        () => divotionAllTime.value > 0
    ),
    createAchievement(
        'Workforce',
        'Own at least one of every worker',
        () => inventory.value.reduce((isUnlocked: boolean, item) => item.owned <= 0 ? false : isUnlocked, true),
    ),
]);

function createAchievement(name: string, description: string, isUnlocked: () => boolean): Achievement {
    const unlocked = computed(isUnlocked);

    watch(unlocked, (value) => {
        if (value) {
            show({ title: `Achievement unlocked: ${name}`, content: description, timeout: 3000 });
        }
    });

    return { name, description, unlocked };
}

export function useAchievements() {
    return {
        achievements,
    };
}
