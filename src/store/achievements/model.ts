import { ComputedRef } from 'vue';

export interface Achievement {
    name: string;
    description: string;
    unlocked: ComputedRef<boolean>;
}
